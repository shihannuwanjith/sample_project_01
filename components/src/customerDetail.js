import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, AsyncStorage, Image, FlatList } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import base64 from 'react-native-base64';

export default class customerDetail extends Component {
  state = {
    name: '',
    address: '',
    contact: '',
    nic: '',
    userName: 'Vidura',
    password: '1234',
    data: []
  }


  componentWillMount() {
    this.gettingCustomers()
    // this.displayData()

  }



  gettingCustomers() {
    fetch("http://5.189.148.181:8080/sales-drive/api/clientDetail?text=&count=20&page=0 ",
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          'Authorization': 'Basic ' + base64.encode(this.state.userName + ":" + this.state.password),
        }
      }
    )
      .then(response => {
        console.log(JSON.stringify(response))
        return response.json()
      })
      .then(responseJson => {
        console.log(JSON.stringify(responseJson))
        this.setState({
          data: responseJson
        })

      })
      .catch((error) => {
        console.log(error);

      });
  } 


  displayData = async () => {
    try {
      let user = await AsyncStorage.getItem('user');
      let parsed = JSON.parse(user);
      // console.log(JSON.stringify(parsed.name));


      this.setState({
        name: parsed.name,
        address: parsed.address,
        contact: parsed.contact,
        nic: parsed.nic
      })
    }
    catch (error) {
      alert(error)
    }
  }


  render() {
    return (
      <View style={styles.View}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }) =>
            <View style={{ backgroundColor: 'White', height: 70, width: 300,marginBottom:10,marginTop:10,
             shadowColor: "White",
             shadowOffset: {
                 width: 0,
                 height: 10,
             },
             shadowOpacity: 0.53,
             shadowRadius: 15.98,
             elevation: 21,}}>
              <Text style={styles.item} style={{color:"black", left:20, top:20,fontSize:20}}>{item.businessName}</Text>
              <Text style={styles.item} style={{color:"black",left:200}}>{item.id}</Text>
              <Image style={{ width: 60, height: 60 , borderRadius:50, right:0 , position:"absolute"}} source={{ uri: 'http://5.189.148.181:8080/sales-drive/' + item.firstImagePath }}></Image>
            </View>
          }

        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  View: {
    backgroundColor: 'white',
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
    height: hp('96%'),
    width: wp('100%')
  },
 });
