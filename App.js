/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { Navigator, StatusBar, Platform, StyleSheet, Text, View, Alert, AsyncStorage } from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


import customerDetails from './components/src/customerDetail';


const RootStack = createStackNavigator({

 
  customerDetails:{
    screen:customerDetails,
    navigationOptions:{header:null}
 },
},

  {
    initialRouteName: 'customerDetails',


  },
  {
    headerMode: 'screen'
  },
);
 
// );

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <AppContainer/>
      </View>
    );
  }


}




//----------------------------------------------------------------------------//

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


  
   


